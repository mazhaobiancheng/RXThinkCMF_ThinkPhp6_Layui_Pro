// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 布局管理
 * @author 牧羊人
 * @since 2020/7/4
 */
layui.use(['form', 'function'], function () {
    var form = layui.form,
        func = layui.function,
        $ = layui.$;

    if (A == 'index') {
        //【TABLE列数组】
        var cols = [
            {type: 'checkbox', fixed: 'left'}
            , {field: 'id', width: 80, title: 'ID', align: 'center', sort: true, fixed: 'left'}
            , {field: 'title', width: 200, title: '布局标题', align: 'center'}
            , {field: 'loc_name', width: 250, title: '推荐位置编号', align: 'center'}
            , {field: 'image_url', width: 60, title: '封面', align: 'center', templet: function (d) {
                    return '<a href="' + d.image_url + '" target="_blank"><img src="' + d.image_url + '" height="26" /></a>';
                }
            }
            , {field: 'type_name', width: 100, title: '推荐类型', align: 'center'}
            , {field: 'content', width: 350, title: '推荐内容', align: 'center'}
            , {field: 'item_name', width: 100, title: '所属站点', align: 'center'}
            , {field: 'sort', width: 100, title: '排序', align: 'center'}
            , {field: 'create_time', width: 180, title: '创建时间', align: 'center', sort: true}
            , {field: 'update_time', width: 180, title: '更新时间', align: 'center', sort: true}
            , {fixed: 'right', width: 150, title: '功能操作', align: 'center', toolbar: '#toolBar'}
        ];

        //【TABLE渲染】
        func.tableIns(cols, "tableList");

        //【设置弹框】
        func.setWin("布局");

    } else {
        //监听推荐类型
        var type = $("#type").val();
        var typeStr = '';
        form.on('select(type)', function (data) {
            type = data.value;
            typeStr = data.elem[data.elem.selectedIndex].text;
        });

        //选择类型对象
        $("#type_value").click(function () {
            //推荐类型
            var title, url;
            if (type == 1) {
                //CMS文章
                title = "请选择推荐模块";
                url = mUrl + "/article/index/?simple=1";
            } else {
                //其他

            }

            if (!url) {
                layer.msg("请选择类型");
                return false;
            }

            //【弹开窗体】
            func.showWin("选择内容", url, 1000, 600);
        });
    }
});

